import sqlite3
c=sqlite3.connect('Database.sqlite')
cur=c.cursor()
# Print the names of both the Home Teams and Away Teams in each match played in 2015 and Full time Home Goals (FTHG) = 5
cur.execute("SELECT HomeTeam, AwayTeam FROM Matches WHERE Season=2015 AND FTHG=5")
print("HomeTeam and AwayTeam in table Matches")
for item in cur.fetchall():
    print(item)

# Print the details of the matches where Arsenal is the Home Team and  Full Time Result (FTR) is “A” (Away Win)
cur.execute("SELECT * FROM Matches WHERE HomeTeam='Arsenal' AND FTR='A'")
print("Table Matches with Arsenal as HomeTeam")
for item in cur.fetchall():
    print(item)

# Print all the matches from the 2012 season till the 2015 season where Away Team is Bayern Munich and Full time Away Goals (FTHG) > 2
cur.execute("SELECT * FROM Matches WHERE Season>=2012 AND Season<=2015 AND AwayTeam='Bayern Munich' AND FTHG>2")
print("Table Matches seasons 2012 to 2015 with Bayern Munich as AwayTeam")
for item in cur.fetchall():
    print(item)

# Print all the matches where the Home Team name begins with “A” and Away Team name begins with “M”
cur.execute("SELECT * FROM Matches WHERE HomeTeam LIKE 'A%' AND AwayTeam LIKE 'M%' ")
print("Table Matches with HomeTeam starting with 'A' and AwayTeam starting with 'M'")
for item in cur.fetchall():
    print(item)

c.commit()
c.close()
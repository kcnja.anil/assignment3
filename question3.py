import sqlite3
c=sqlite3.connect('Database.sqlite')
cur=c.cursor()

# Write a query that returns the HomeTeam, FTHG and FTAG from the Matches table in 2010 Season and Aachen Home Team, in decreasing order of FTHG
cur.execute("SELECT HomeTeam, FTHG, FTAG FROM Matches WHERE Season=2010 AND HomeTeam='Aachen' ORDER BY FTHG DESC")
for item in cur.fetchall():
    print(item)

# Print the total number of home games each team won during the 2016 season in descending order of number of home games from the Matches table
cur.execute("SELECT HomeTeam FROM Matches WHERE Season= 2016 AND FTR = 'H'")
list_item =[]
for item, in cur.fetchall():
    list_item.append(item)
desc_order = sorted(set(list_item),key = lambda val: -(list_item.count(val)))
print("Order of games won in descending order: ",desc_order)

# Write a query that returns the first ten rows from the Unique_Teams table
cur.execute("SELECT * FROM Unique_Teams WHERE rowid<10")
print("First ten rows from Unique_Teams: ")
for item in cur.fetchall():
    print(item)

# Print the Match_ID and Unique_Team_ID with the corresponding Team_Name from the Unique_Teams and Teams_in_Matches tables
cur.execute("SELECT Match_ID, Teams_in_Matches.Unique_Team_ID, TeamName FROM Unique_Teams INNER JOIN Teams_in_Matches ON Unique_Teams.Unique_Team_ID  = Teams_in_Matches.Unique_Team_ID")
for item in cur.fetchall():
    print(item)

# Write a query that joins together the Unique_Teams data table and the Teams table, and returns the first 10 rows
cur.execute("SELECT * FROM Teams INNER JOIN Unique_Teams ON Teams.TeamName=Unique_Teams.TeamName LIMIT 10")
print("First ten rows from Unique_Teams and Teams: ")
for item in cur.fetchall():
    print(item)

# Write a query showing the Unique_Team_ID and TeamName from the Unique_Teams table and AvgAgeHome, Season and ForeignPlayersHome from the Teams table.Return the first five rows
cur.execute("SELECT Unique_Team_ID, Unique_Teams.TeamName,AvgAgeHome, Season, ForeignPlayersHome FROM Teams INNER JOIN Unique_Teams ON Teams.TeamName=Unique_Teams.TeamName LIMIT 5")
print("First five rows from Unique_Teams and Teams: ")
for item in cur.fetchall():
    print(item)

# Write a query that shows the highest Match_ID for each team that ends in a “y” or a “r”. Along with the maximum Match_ID, display the Unique_Team_ID from the Teams_in_Matches table and the TeamName from the Unique_Teams table
cur.execute("SELECT (Match_ID), Teams_in_Matches.Unique_Team_ID, TeamName FROM Unique_Teams INNER JOIN Teams_in_Matches ON Teams_in_Matches.Unique_Team_ID= Unique_Teams.Unique_Team_ID WHERE TeamName LIKE '%y' OR TeamName LIKE '%r' GROUP BY TeamName")
print("Unique_Teams and Teams_in_Matches: ")
for item in cur.fetchall():
    print(item)

c.commit()
c.close()
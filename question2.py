import sqlite3
c=sqlite3.connect('Database.sqlite')
cur=c.cursor()

# Counts all the rows in the Teams table
cur.execute("SELECT COUNT(*) FROM Teams")
print("The number of rows: ",cur.fetchall())

# Print all the unique values that are included in the Season column in the Teams table
cur.execute("SELECT DISTINCT Season FROM Teams")
print("The unique values in Season column: ",cur.fetchall())

# Print the largest and smallest stadium capacity that is included in the Teams table
cur.execute("SELECT MIN(StadiumCapacity),MAX(StadiumCapacity) FROM Teams")
item=cur.fetchone()
print("The minimum stadium capacity is: ", item[0],"\nThe maximum stadium capacity is: ", item[1])

# Print the sum of squad players for all teams during the 2014 season from the Teams table
cur.execute("SELECT KaderHome FROM Teams WHERE Season='2014'")
a=0
for item in cur.fetchall():
    item=int(item[0])
    a=a+item
print("Sum of squad player for all teams during 2014 season: ",a)

# Query the Matches table to know how many goals did Man United score during home games on average
cur.execute("SELECT FTHG FROM Matches WHERE HomeTeam='Man United'")
a=0
count=0
for item in cur.fetchall():
    item=int(item[0])
    a=a+item
    count+=1
print("Average goals by Man United during home games: ", str(a/count)[:4])

c.commit()
c.close()
